@extends('layout.master')
@section('content')
<div style="padding: 1em;">    
    <a href="/cast/create" class="btn btn-primary">Tambah</a><br><br>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td style="display: flex;">
                            <form action="/cast/{{$value->id}}" method="GET" style="margin-right: 1em;">
                                <input type="submit" class="btn btn-info" value="Show">
                            </form>
                            <form action="/cast/{{$value->id}}/edit" method="GET" style="margin-right: 1em;">
                                <input type="submit" class="btn btn-primary" value="Edit">
                            </form>
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
</div>
@endsection