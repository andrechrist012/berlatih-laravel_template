@extends('layout.master')
@section('content')
<div style="padding: 2em;">
    <h2>Show Cast</h2><br>
    <div style="padding: 2em; background-color: white;">
        <h4>Nama: {{$cast->nama}}</h4>
        <h4>Umur: {{$cast->umur}}</h4>
        <h4>Bio: {{$cast->bio}}</h4>
    </div>
    <br><br>
    <a href="/cast" class="btn btn-info">Back</a>
    <a href="/cast/{{$cast->id}}/edit" class="btn btn-primary">Edit</a>
</div>
@endsection